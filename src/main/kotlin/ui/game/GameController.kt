package ui.game

import tornadofx.Controller
import kotlin.random.Random

class GameController: Controller() {

    val isStarted: Boolean
        get() = fields.isNotEmpty()

    private val fields = mutableListOf<FieldMeta>()
    private lateinit var level: Level

    fun start(level: Level, initialFieldIndex: Int) {
        this.level = level
        fields.clear()
        val fieldsCount = level.width * level.height
        repeat(fieldsCount) {
            fields.add(FieldMeta())
        }
        List(level.minesCount) {
            var minedIndex: Int
            do {
                minedIndex = Random.nextInt(fieldsCount)
            } while (minedIndex == initialFieldIndex)
            minedIndex
        }.forEach { minedIndex ->
            fields[minedIndex] = fields[minedIndex].copy(isSafe = false)
        }
        fields.forEachIndexed { index, field ->
            if (field.isSafe) {
                var unsafeCount = 0
                getNeighboursIndexes(index).forEach { neighbourIndex ->
                    if (!fields[neighbourIndex].isSafe) {
                        unsafeCount++
                    }
                }
                fields[index] = field.copy(closeMinesCount = unsafeCount)
            }
        }
    }

    fun toggleMark(index: Int): FieldMeta {
        val field = fields[index]
        if (!field.isClicked) {
            fields[index] = field.copy(isMarked = !field.isMarked)
        }
        return fields[index]
    }

    fun getField(index: Int): FieldMeta {
        val field = fields[index]
        if (!field.isMarked) {
            fields[index] = field.copy(isClicked = true)
        }
        return field
    }

    fun getNeighboursIndexes(index: Int): List<Int> {
        if (!isStarted) {
            return listOf()
        }
        val result = mutableListOf<Int>()
        val isLeftAvailable = index % level.width > 0
        val isRightAvailable = index % level.width < level.width - 1
        val isTopAvailable = index / level.width > 0
        val isBottomAvailable = index / level.width < level.height - 1
        if (isLeftAvailable) {
            if (isTopAvailable) result.add(index - level.width - 1)
            if (isBottomAvailable) result.add(index + level.width - 1)
            result.add(index - 1)
        }
        if (isRightAvailable) {
            if (isTopAvailable) result.add(index - level.width + 1)
            if (isBottomAvailable) result.add(index + level.width + 1)
            result.add(index + 1)
        }
        if (isBottomAvailable) result.add(index + level.width)
        if (isTopAvailable) result.add(index - level.width)
        return result
    }

    fun getCorrectlyMarkedIndexes(): List<Int> =
        fields.filter { it.isMarked && !it.isSafe }.mapIndexed { index, _ -> index }

    fun getNotMarkedIndexes(): List<Int> =
        fields.filter { !it.isMarked && !it.isSafe }.mapIndexed { index, _ -> index }

    data class FieldMeta(
        val isSafe: Boolean = true,
        val isMarked: Boolean = false,
        val isClicked: Boolean = false,
        val closeMinesCount: Int = 0
    )

    enum class Level(
        val label: String,
        val width: Int,
        val height: Int,
        val minesCount: Int
    ) {
        ULTRA_BEGINNER("über n00b", 25, 25, 5),
        BEGINNER("n00b", 8, 8, 10),
        ADVANCED("Mid", 16, 16, 40),
        EXPERT("Pr0", 30, 16, 99)
    }
}