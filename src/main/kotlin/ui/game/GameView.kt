package ui.game

import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.input.MouseButton
import tornadofx.*

class GameView : View() {

    val level: GameController.Level by param(GameController.Level.BEGINNER)

    private val engine: GameController by inject()
    private val buttons = mutableListOf<Button>()

    private var isGameOver = false

    override val root = gridpane {
        alignment = Pos.CENTER
        repeat(level.height) { rowIndex ->
            row {
                repeat(level.width) { columnIndex ->
                    val fieldBtn = button() {
                        prefWidth = 30.0
                        prefHeight = 30.0
                        val fieldIndex = rowIndex * level.width + columnIndex
                        setOnMouseClicked { event ->
                            when(event.button) {
                                MouseButton.PRIMARY -> checkField(this, fieldIndex)
                                MouseButton.SECONDARY -> toggleMark(this, fieldIndex)
                                else -> {
                                    // Do nothing
                                }
                            }
                        }
                    }
                    buttons.add(fieldBtn)
                    add(fieldBtn)
                }
            }
        }
    }

    override fun onDock() {
        super.onDock()
        title = "Saper - ${level.label}"
        primaryStage.width = 30.0 * level.width
        primaryStage.height = 30.0 * level.height
    }

    private fun checkField(button: Button, index: Int) {
        if(!engine.isStarted) {
            engine.start(level, index)
        }
        val result = engine.getField(index)
        if(!result.isClicked && !result.isMarked) {
            if(result.isSafe) {
                println(result.closeMinesCount)
                button.text = result.closeMinesCount.takeIf { it > 0 }?.toString() ?: run {
                    engine.getNeighboursIndexes(index).forEach { neighbourIndex ->
                        checkField(buttons[neighbourIndex], neighbourIndex)
                    }
                    ""
                }
            } else {
                button.text = "X"
                if(!isGameOver) {
                    isGameOver = true
                    alert(header = "Przegranko", content = "No nie udało się", type = Alert.AlertType.INFORMATION)
                    showAll()
                }
            }
            button.isDisable = true
        }
    }

    private fun showAll() {
        repeat(level.height * level.width) {index ->
            checkField(buttons[index], index)
        }
    }

    private fun toggleMark(button: Button, index: Int) {
        if(!engine.isStarted) {
            return
        }
        val result = engine.toggleMark(index)
        if(!result.isClicked) {
            if(result.isMarked) {
                button.text = "!"
            } else {
                button.text = ""
            }
        }
    }
}