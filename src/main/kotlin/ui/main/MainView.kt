package ui.main

import ui.game.GameController
import javafx.geometry.Pos
import tornadofx.*
import ui.game.GameView

class MainView : View("Saper") {

    override val root = vbox {
        alignment = Pos.CENTER
        spacing = 10.0
        label("Wybierz poziom trudności")
        GameController.Level.values().forEach { level ->
            add(buildLevelButton(level))
        }
    }

    override fun onDock() {
        super.onDock()
        primaryStage.width = 200.0
        primaryStage.height = 300.0
    }

    private fun buildLevelButton(level: GameController.Level) = button(level.label) {
        action {
            val view = find<GameView>(mapOf(GameView::level to level))
            replaceWith(view)
        }
    }
}